﻿

using System.Web.Optimization;

namespace Versa.App_Start
{
    public class BundleConfig
    {


        public static void RegisterBundles(BundleCollection bundles)
        {


            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                    "~/Versa/Scripts/bootstrap.min.js"
                ));

            bundles.Add(new ScriptBundle("~/bundles/js").Include(
                    "~/js/jquery-1.11.3.min.js",
                    "~/js/jquery-migrate-1.2.1.min.js",
                    "~/rs-plugin/js/jquery.themepunch.tools.min.js",
                    "~/rs-plugin/js/jquery.themepunch.revolution.min.js",
                    "~/js/jquery.ba-bbq.min.js",
                    "~/js/jquery-ui-1.11.4.custom.min.js",
                    "~/js/jquery.ui.touch-punch.min.js",
                    "~/js/jquery.isotope.min.js",
                    "~/js/jquery.easing.1.3.min.js",
                    "~/js/jquery.carouFredSel-6.2.1-packed.js",
                    "~/js/jquery.touchSwipe.min.js",
                    "~/js/jquery.transit.min.js",
                    "~/js/jquery.hint.min.js",
                    "~/js/jquery.costCalculator.min.js",
                    "~/js/jquery.parallax.min.js",
                    "~/js/jquery.prettyPhoto.js",
                    "~/js/jquery.qtip.min.js",
                    "~/js/jquery.blockUI.min.js",
                    "~/js/main.js",
                    "~/js/odometer.min.js",
                    "~/cabecalho/js/app.js",
                    "~/cabecalho/js/sticky.js"
                ));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                    "~/js/jquery.validate*"
                ));

            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                    "~/js/modernizr-2.8.3.js"
                ));


            bundles.Add(new StyleBundle("~/Content/css").Include(
                    "~/Content/wpforms/css/wpforms-full.css",
                    "~/rs-plugin/css/settings.css",
                    "~/Content/js_composer.min.css",
                    "~/style/Custom.css",
                    "~/style/reset.css",
                    "~/style/superfish.css",
                    "~/style/prettyPhoto.css",
                    "~/style/jquery.qtip.css",
                    "~/style/style.css",
                    "~/style/animations.css",
                    "~/style/responsive.css",
                    "~/style/odometer-theme-default.css",
                    "~/fonts/streamline-small/styles.css",
                    "~/fonts/streamline-large/styles.css",
                    "~/fonts/template/styles.css",
                    "~/fonts/social/styles.css",
                    "~/cabecalho/css/header/main.css",
                    "~/cabecalho/vendor/sticky.css",
                    "~/cabecalho/css/font-awesome.min.css",
                    "~/cabecalho/StyleSheet1.css"
                ));

            bundles.Add(new StyleBundle("~/Content/bootstrap").Include(
                "~/Content/bootstrap.min.css"
                ));


            BundleTable.EnableOptimizations = false;


        }

    }
}