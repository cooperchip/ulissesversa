﻿using System.ComponentModel.DataAnnotations;

namespace Versa.Identity.ViewModels
{
    /// <summary>
    /// 
    /// </summary>
    public class LoginViewModel
    {
        /// <summary>
        /// 
        /// </summary>        
        [Required]
        [Display(Name = "Apelido")]
        [MaxLength(16, ErrorMessage = "Máximo de Caracter Permitido: 16")]
        public string Apelido { get; set; }


        /// <summary>
        /// 
        /// </summary>
        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Senha")]
        public string Password { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Display(Name = "Lembar?")]
        public bool RememberMe { get; set; }
    }

}