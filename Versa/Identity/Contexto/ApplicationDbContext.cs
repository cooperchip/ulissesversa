﻿
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Data.Entity;
using Versa.Identity.ViewModels;

namespace Versa.Identity.Contexto
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>, IDisposable
    {

        public ApplicationDbContext() : base("SchoolContext")
        {
            Configuration.LazyLoadingEnabled = false;
            Configuration.ProxyCreationEnabled = false;
        }

        public DbSet<Client> Client { get; set; }
        public DbSet<Claims> Claims { get; set; }


        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

    }
}

