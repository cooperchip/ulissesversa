﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Versa.Domain;
using Versa.Infra.Data.Contexto;

namespace Versa.Controllers
{
    public class AdministradoraController : Controller
    {
        private VersaDbContext db = new VersaDbContext();

        // GET: Administradora
        public async Task<ActionResult> Index()
        {
            return View(await db.Administradora.ToListAsync());
        }

        // GET: Administradora/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Administradora administradora = await db.Administradora.FindAsync(id);
            if (administradora == null)
            {
                return HttpNotFound();
            }
            return View(administradora);
        }

        // GET: Administradora/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Administradora/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "AdministradoraId,Descricao")] Administradora administradora)
        {
            if (ModelState.IsValid)
            {
                db.Administradora.Add(administradora);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(administradora);
        }

        // GET: Administradora/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Administradora administradora = await db.Administradora.FindAsync(id);
            if (administradora == null)
            {
                return HttpNotFound();
            }
            return View(administradora);
        }

        // POST: Administradora/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "AdministradoraId,Descricao")] Administradora administradora)
        {
            if (ModelState.IsValid)
            {
                db.Entry(administradora).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(administradora);
        }

        // GET: Administradora/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Administradora administradora = await db.Administradora.FindAsync(id);
            if (administradora == null)
            {
                return HttpNotFound();
            }
            return View(administradora);
        }

        // POST: Administradora/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Administradora administradora = await db.Administradora.FindAsync(id);
            db.Administradora.Remove(administradora);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
