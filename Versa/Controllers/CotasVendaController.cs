﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Versa.Domain;
using Versa.Infra.Data.Contexto;
using Versa.Identity.Permissions;

namespace Versa.Controllers
{
    public class CotasVendaController : Controller
    {
        private VersaDbContext db = new VersaDbContext();

        [Authorize(Roles = "Admin")]
        public async Task<ActionResult> Index()
        {
            var cotasVenda = db.CotasVenda.Include(c => c.Administradora);
            return View(await cotasVenda.ToListAsync());
        }

        // GET: CotasVenda/Details/5
        public async Task<ActionResult> Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CotasVenda cotasVenda = await db.CotasVenda.FindAsync(id);
            if (cotasVenda == null)
            {
                return HttpNotFound();
            }
            return View(cotasVenda);
        }



        [Authorize(Roles = "Admin")]        
        public ActionResult Create()
        {
            ViewBag.AdministradoraId = new SelectList(db.Administradora, "AdministradoraId", "Descricao");
            return View();
        }

        // POST: CotasVenda/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,Credito,Parcela,NumParcela,Valor,Status,Vencimento,TipoCota,AdministradoraId")] CotasVenda cotasVenda)
        {
            if (ModelState.IsValid)
            {
                cotasVenda.Id = Guid.NewGuid();
                db.CotasVenda.Add(cotasVenda);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.AdministradoraId = new SelectList(db.Administradora, "AdministradoraId", "Descricao", cotasVenda.AdministradoraId);
            return View(cotasVenda);
        }

        // GET: CotasVenda/Edit/5
        public async Task<ActionResult> Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CotasVenda cotasVenda = await db.CotasVenda.FindAsync(id);
            if (cotasVenda == null)
            {
                return HttpNotFound();
            }
            ViewBag.AdministradoraId = new SelectList(db.Administradora, "AdministradoraId", "Descricao", cotasVenda.AdministradoraId);
            return View(cotasVenda);
        }

        // POST: CotasVenda/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,Credito,Parcela,NumParcela,Valor,Status,Vencimento,TipoCota,AdministradoraId")] CotasVenda cotasVenda)
        {
            if (ModelState.IsValid)
            {
                db.Entry(cotasVenda).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.AdministradoraId = new SelectList(db.Administradora, "AdministradoraId", "Descricao", cotasVenda.AdministradoraId);
            return View(cotasVenda);
        }

        // GET: CotasVenda/Delete/5
        public async Task<ActionResult> Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CotasVenda cotasVenda = await db.CotasVenda.FindAsync(id);
            if (cotasVenda == null)
            {
                return HttpNotFound();
            }
            return View(cotasVenda);
        }

        // POST: CotasVenda/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(Guid id)
        {
            CotasVenda cotasVenda = await db.CotasVenda.FindAsync(id);
            db.CotasVenda.Remove(cotasVenda);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }


        [AllowAnonymous]
        public ActionResult ListaCotas(TipoCota tipo)
        {
            var cotasVenda = db.CotasVenda.Include(c => c.Administradora);
            var cotas = cotasVenda.Where(c => c.TipoCota == tipo);

           return PartialView("ListaCotas", cotas.ToList());
        }

    }
}
