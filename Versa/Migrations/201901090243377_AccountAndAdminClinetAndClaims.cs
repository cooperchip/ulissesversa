namespace Versa.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class AccountAndAdminClinetAndClaims : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AspNetClaims",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.AspNetClients",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ClientKey = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.AspNetUsers", "Apelido", c => c.String(nullable: false, maxLength: 16));
            AddColumn("dbo.AspNetUsers", "NomeCompleto", c => c.String(nullable: false, maxLength: 35));
            AddColumn("dbo.AspNetUsers", "Twitter", c => c.String(maxLength: 120));
            AddColumn("dbo.AspNetUsers", "Skype", c => c.String(maxLength: 120));
            AddColumn("dbo.AspNetUsers", "AboutMe", c => c.String());
            DropColumn("dbo.AspNetUsers", "FirstName");
            DropColumn("dbo.AspNetUsers", "LastName");
        }
        
        public override void Down()
        {
            AddColumn("dbo.AspNetUsers", "LastName", c => c.String());
            AddColumn("dbo.AspNetUsers", "FirstName", c => c.String());
            DropColumn("dbo.AspNetUsers", "AboutMe");
            DropColumn("dbo.AspNetUsers", "Skype");
            DropColumn("dbo.AspNetUsers", "Twitter");
            DropColumn("dbo.AspNetUsers", "NomeCompleto");
            DropColumn("dbo.AspNetUsers", "Apelido");
            DropTable("dbo.AspNetClients");
            DropTable("dbo.AspNetClaims");
        }
    }
}
