namespace Versa.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddCotasVenda : DbMigration
    {
        public override void Up()
        {
            DropTable("dbo.CotasVenda");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.CotasVenda",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Credito = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Parcela = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Parcelas = c.Int(nullable: false),
                        Valor = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Administradora = c.String(),
                        Status = c.String(),
                        Vencimento = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
        }
    }
}
