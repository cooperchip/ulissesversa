

using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Data.Entity.Migrations;
using System.Linq;
using Versa.Identity;
using Versa.Identity.Contexto;

namespace Versa.Migrations
{
    internal sealed class Configuration : DbMigrationsConfiguration<ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(ApplicationDbContext context)
        {
            const string nomecompleto = "Administrador do Sistema";
            const string apelido = "Administrador";
            const string email = "admin@admin.com";
            const string password = "Admin@123456";
            const string roleName = "Admin";

            if (!context.Roles.Any(r => r.Name == roleName))
            {
                var store = new RoleStore<IdentityRole>(context);
                var manager = new RoleManager<IdentityRole>(store);
                var role = new IdentityRole { Name = roleName };
                manager.Create(role);
            }

            if (!context.Users.Any(u => u.UserName == apelido))
            {
                var store = new UserStore<ApplicationUser>(context);
                var manager = new UserManager<ApplicationUser>(store);
                var user = new ApplicationUser
                {
                    UserName = apelido,
                    Apelido = apelido,
                    Email = email,
                    NomeCompleto = nomecompleto
                };

                manager.Create(user, password);
                manager.AddToRole(user.Id, roleName);
            }

            base.Seed(context);
        }
    }
}
