﻿

using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using Versa.Domain;

namespace Versa.Infra.Data.Contexto
{
    public class VersaDbContext : DbContext    
    {
        public VersaDbContext()
            :base("SchoolContext")
        {

        }

        public DbSet<CotasVenda> CotasVenda { get; set; }
        public DbSet<Administradora> Administradora { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
            modelBuilder.Conventions.Remove<ManyToManyCascadeDeleteConvention>();
            base.OnModelCreating(modelBuilder);
        }

    }
}
