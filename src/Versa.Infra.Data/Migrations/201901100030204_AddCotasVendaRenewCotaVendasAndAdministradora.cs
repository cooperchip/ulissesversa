namespace Versa.Infra.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddCotasVendaRenewCotaVendasAndAdministradora : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Administradora",
                c => new
                    {
                        AdministradoraId = c.Int(nullable: false, identity: true),
                        Descricao = c.String(nullable: false, maxLength: 30),
                    })
                .PrimaryKey(t => t.AdministradoraId);
            
            CreateTable(
                "dbo.CotasVenda",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Credito = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Parcela = c.Decimal(nullable: false, precision: 18, scale: 2),
                        NumParcela = c.Int(nullable: false),
                        Valor = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Status = c.Boolean(nullable: false),
                        Vencimento = c.DateTime(nullable: false),
                        TipoCota = c.Int(nullable: false),
                        AdministradoraId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Administradora", t => t.AdministradoraId, cascadeDelete: false)
                .Index(t => t.AdministradoraId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.CotasVenda", "AdministradoraId", "dbo.Administradora");
            DropIndex("dbo.CotasVenda", new[] { "AdministradoraId" });
            DropTable("dbo.CotasVenda");
            DropTable("dbo.Administradora");
        }
    }
}
