namespace Versa.Infra.Data.Migrations
{
    using System.Data.Entity.Migrations;
    using Versa.Infra.Data.Contexto;

    internal sealed class Configuration : DbMigrationsConfiguration<VersaDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(VersaDbContext context)
        {

        }
    }
}
