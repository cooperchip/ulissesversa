namespace Versa.Infra.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddCotasVendaNew : DbMigration
    {
        public override void Up()
        {
            DropTable("dbo.CotasVenda");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.CotasVenda",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Credito = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Parcela = c.Decimal(nullable: false, precision: 18, scale: 2),
                        NumParcela = c.Int(nullable: false),
                        Valor = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Administradora = c.String(nullable: false, maxLength: 60),
                        Status = c.Boolean(nullable: false),
                        Vencimento = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
    }
}
