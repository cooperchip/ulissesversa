namespace Versa.Infra.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddCotasVendaRenewCotaVendasAndAdministradoraChange : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.CotasVenda", "AdministradoraId", "dbo.Administradora");
            AddForeignKey("dbo.CotasVenda", "AdministradoraId", "dbo.Administradora", "AdministradoraId");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.CotasVenda", "AdministradoraId", "dbo.Administradora");
            AddForeignKey("dbo.CotasVenda", "AdministradoraId", "dbo.Administradora", "AdministradoraId", cascadeDelete: true);
        }
    }
}
