﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Versa.Domain
{
    public class CotasVenda
    {

        public CotasVenda()
        {
            Id = Guid.NewGuid();
        }

        [Key]
        public Guid Id { get; set; }

        [DataType(DataType.Currency)]
        [Display(Name = "Crédito")]
        public decimal Credito { get; set; }


        [DataType(DataType.Currency)]
        [Display(Name = "Entrada")]
        public decimal Parcela { get; set; }

        [Display(Name = "Nº de Parcelas")]
        public int NumParcela { get; set; }


        [DataType(DataType.Currency)]
        public decimal Valor { get; set; }


        public bool Status { get; set; }

        [DataType(DataType.DateTime)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime Vencimento { get; set; }


        public virtual TipoCota TipoCota { get; set; }

        [ForeignKey("Administradora")]
        [Display(Name = "Administradora")]
        public int AdministradoraId { get; set; }

        public virtual Administradora Administradora { get; set; }

        [NotMapped]
        public virtual IEnumerable<CotasVenda> ListarcotasVendas { get; }

    }
}