﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Versa.Domain
{
    public class Administradora
    {

        [Key]
        public int AdministradoraId { get; set; }

        [Display(Name = "Administradora")]
        [Required(ErrorMessage = "Campo Obrigatório!")]
        [StringLength(30, ErrorMessage = "Máximo de caracteres permitidos: 30")]
        public string Descricao { get; set; }

        public virtual ICollection<CotasVenda> Vendas { get; set; }


    }
}
